#!/usr/bin/env python3

"""
This file provides setup for machine environment for our testing pipeline.
"""

import sys
import traceback
from qecore.sandbox import TestSandbox


def before_all(context) -> None:
    """
    This function will be run once in every 'behave' command called.
    """

    try:
        context.sandbox = TestSandbox("evolution", context=context)

        context.evolution = context.sandbox.get_application(
            name="evolution", desktop_file_name="org.gnome.Evolution"
        )

        context.terminal = context.sandbox.get_application(
            name="gnome-terminal",
            a11y_app_name="gnome-terminal-server",
            desktop_file_name="org.gnome.Terminal.desktop",
        )
        context.terminal.exit_shortcut = "<Ctrl><Shift><Q>"

        context.preferences = context.sandbox.get_application(
            name="gnome-terminal",
            a11y_app_name="gnome-terminal-preferences",
            desktop_file_name="org.gnome.Terminal.Preferences.desktop",
        )

        context.settings = context.sandbox.get_application(
            name="gnome-control-center",
            desktop_file_name="gnome-control-center"
        )

        context.zenity = context.sandbox.get_application(
            name="zenity",
            desktop_file_exists=False
        )


    except Exception as error:  # pylint: disable=broad-except
        print(f"Environment error: before_all: {error}")
        traceback.print_exc(file=sys.stdout)

        # Save the traceback so that we can use it later when we have html report.
        context.failed_setup = traceback.format_exc()


def before_scenario(context, scenario) -> None:
    """
    This function will be run before every scenario in 'behave' command called.
    """

    try:
        context.sandbox.before_scenario(context, scenario)

    except Exception as error:  # pylint: disable=broad-except
        print(f"Environment error: before_scenario: {error}")
        traceback.print_exc(file=sys.stdout)

        # Attach failed setup from Before Scenario to our html-pretty report.
        context.embed("text", traceback.format_exc(), "Failed setup in Before Scenario")

        # Recommended to correctly place the embed to the Before Scenario.
        sys.exit(1)


def after_scenario(context, scenario) -> None:
    """
    This function will be run after every scenario in 'behave' command called.
    """

    try:
        context.sandbox.after_scenario(context, scenario)

    except Exception as error:  # pylint: disable=broad-except
        print(f"Environment error: after_scenario: {error}")
        traceback.print_exc(file=sys.stdout)

        # Attach failed setup from After Scenario to our html-pretty report.
        context.embed("text", traceback.format_exc(), "Failed cleanup in After Scenario")
