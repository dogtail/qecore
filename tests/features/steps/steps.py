from behave import step
from time import sleep
import qecore.common_steps
from dogtail.rawinput import pressKey


@step('Skip with message "{msg}"')
def skip_step(context, msg):
    context.scenario.skip(msg)


@step("Make sure Menubar is showing")
def make_sure_menubar_is_showing(context):
    menubar = context.terminal.instance.child(roleName="menu bar")
    for _ in range(10):
        if not (menubar.showing and menubar.visible):
            terminal_frame = context.terminal.instance.child(roleName="frame")
            sleep(1)
            terminal_frame.click(3)
            try:
                context.execute_steps('* Left click "Show Menubar" "check menu item" in "terminal"')
            except Exception:
                pressKey("Esc")

        else:
            return
