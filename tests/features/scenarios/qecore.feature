@qecore_feature
Feature: qecore test

  @click_gnome_shell_activities
  Scenario: Click Activities in GNOME
    * Left click "Activities" "label" in "gnome-shell"


  @fail_click_gnome_shell_activities_button
  Scenario: Fail to click Activities in GNOME
    * Left click "Activities" "labels" in "gnome-shell"


  @undefined_step
  Scenario: Undefined step
    * This step is not defined


  @skip_step
  Scenario: Skip inside step
    * Skip with message "this test is skipped"


  @skip_before_scenario
  Scenario: Skip inside before_scenario
    * This step should not be reached


  @test_start_zenity
  Scenario: Start Application via command.
    * Start application "zenity" via command "zenity --error"


  @test_start_settings
  Scenario: Start Application via command.
    * Start application "settings" via "command"


  @start_via_command
  Scenario: Start application via command.
    * Start application "terminal" via "command"


  @start_via_menu
  Scenario: Start application via menu.
    * Start application "terminal" via "menu"


  @test_some_common_steps
  Scenario: General common step usage with multiple variations.
    * Start application "terminal" via "command"
    * Wait 3 seconds before action
    * Make sure Menubar is showing

    * Left click "File" "menu"
    * Left click "Edit" "menu" in "terminal"
    * Left click "View" "menu" that is "showing"
    * Left click "Search" "menu" that is "visible" in "terminal"

    * Mouse over "Terminal" "menu" that is "visible" in "terminal"

    * Make an action "click" for "Help" "menu" that is "visible" in "terminal"

    * Item "File" "menu" found
    * Item "Edit" "menu" with description "Empty" found
    * Item "View" "menu" with description "Empty" is "showing" found in "terminal"
    * Item "ViewNotFound" "menu" was not found in "terminal"
    * Item "Terminal" "terminal" has text "test@"
    * Item "Terminal" "terminal" does not have text "WHAT"
    * Item "Terminal" "terminal" does not have description "WHAT"
    * Item "Search" "menu" is "visible"
    * Item "Terminal" "menu" is "visible" in "terminal"
    * Item "Help" "menu" is not "checked"

    * Press key: "Esc"
    * Key combo: "<Super><Up>"
    * Key combo: "<Shift><Control><F>"
    * Item "Find" "frame" that is at least "-30" from topleft is "visible" in "terminal"
    * Item "Find" "frame" that is at most "500" from topleft is "visible" in "terminal"
    * Item "Find" "frame" has size at least "10" is "visible" in "terminal"
    * Item "Find" "frame" has size at most "500" is "visible" in "terminal"
    * Press key: "Esc"

    * Application "terminal" is running
    * Close application "terminal" via "gnome panel"
    * Application "terminal" is no longer running

    # does not work in RHEL8
    #* Start application "terminal" via "command"
    #* Close application "terminal" via "application panel menu"

    * Start application "terminal" via "command"
    * Close application "terminal" via "shortcut"

    * Start application "terminal" via "command"
    * Close application "terminal" via "kill command"

    * Start application "terminal" via "command"
    * Make sure Menubar is showing
    * Close application "terminal" via "menu:File:Close Window"
