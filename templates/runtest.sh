#!/bin/bash
set -x

RUN_START=$(date +%s)
COREDUMP_TARGET="<tested_component>"
TEST_REPORT_FILE="/tmp/report_$TEST.html"

# Reducing false negatives by repeating tests but not those that are expected to fail.
NON_REPEATING_TESTS=""
is_known_to_fail() {
  [[ $NON_REPEATING_TESTS =~ (^|[[:space:]])$1($|[[:space:]]) ]] && echo "0" || echo "1"
}


# Setup qecore.
if [ ! -e /tmp/qecore_setup_done ]; then
  # Remove any DesktopQE installed behave, qecore will pull upstream version.
  dnf -y erase python3-behave python2-behave
  # Specify version, in case of a mistake in the next version, we still need working automation.
  python3 -m pip install qecore==3.29
  # Soft link to make qecore-headless script easily accessible.
  ln -s /usr/local/bin/qecore-headless /usr/bin/qecore-headless
  # Make the setup only once.
  touch /tmp/qecore_setup_done
fi


# In attempt to reduce false negatives, repeat tests that are not expected to fail.
MAX_FAIL_COUNT=2
for i in $(seq 1 1 $MAX_FAIL_COUNT); do
  if [[ $(arch) == "x86_64" ]]; then
    # For x86_64 respect the system setting.
    sudo -u test qecore-headless "behave -t $1 -k -f html-pretty -o $TEST_REPORT_FILE -f plain"; rc=$?
  else
    # Fixing xorg on anything else.
    sudo -u test qecore-headless --session-type xorg "behave -t $1 -k -f html-pretty -o $TEST_REPORT_FILE -f plain"; rc=$?
  fi

  [ $rc -eq 0 -o $rc -eq 77 ] && break
  [ "$(is_known_to_fail $1)" -eq 0 ] && break

done


# Mark result FAIL or PASS depending on the test result.
RESULT="FAIL"
if [ $rc -eq 0 ]; then
  RESULT="PASS"
fi


# Find out if there are any coredumps present.
coredumpctl list --since=@$RUN_START; general_dump_rc=$?

# Evaluate the coredumpctl with only relevant data being currently tested component.
coredumpctl list --since=@$RUN_START | grep $COREDUMP_TARGET; targeted_dump_rc=$?

if [ $targeted_dump_rc -eq 0 ]; then
  # Change the reported name to indicate COREDUMP in tested component has occurred.
  echo "COREDUMP_${TEST}" >> /tmp/automation_debug.log

elif [ $general_dump_rc -eq 0 ]; then
  # Change the reported name to indicate that some coredump happened and INVESTIGATION could be needed.
  echo "INVESTIGATE_${TEST}" >> /tmp/automation_debug.log

fi


# Make the actual report.
rhts-report-result $TEST $RESULT $TEST_REPORT_FILE
exit $rc
