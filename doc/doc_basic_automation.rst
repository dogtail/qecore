Automation of Desktop Applications
**********************************

Automation is done using **AT-SPI** (Assistive Technology - Service Provider Interface).

Which is then wrapped by `pyatspi <https://people.gnome.org/~parente/pyatspi/doc/>`_  for use in python.

And all of that is then taken by `dogtail <https://gitlab.com/dogtail/dogtail>`_ which serves as our API through which we test.


.. code-block::

    ┌─────────────────────────────────┐
    │         ┌─────────────────────┐ │
    │         │         ┌────────┐  │ │
    │ dogtail │ pyatspi │ AT-SPI │  │ │
    │         │         └────────┘  │ │
    │         └─────────────────────┘ │
    └─────────────────────────────────┘

.. _dogtail_example:

Dogtail
=======
Installing dogtail via dnf/yum
`dogtail packages <https://vhumpa.fedorapeople.org/dogtail/RHEL8/>`_  (soon also as pip module).

Using dogtail is recommended in remotely connected machine (e.g. VM) and not directly on local machine.
Once in the remote machine you need to run a script provided by qecore that will execute a script
which is a *bash* by default::

    $ qecore-headless

This will adjust the configs for GDM and Accountsservice to enable autologin and use GNOME or Classic GNOME.
Enables the Accessibility Toolkit which is essential for the dogtail to be able to retrieve all data from the session.
And finally it will start GDM and executes bash, from which we can start the test itself.

Simple example to start Terminal and execute the command:

.. code-block:: python

    #!/usr/bin/python3
    from dogtail.tree import root
    from dogtail.rawinput import typeText, pressKey
    from time import sleep

    # First open the application
    pressKey("Super") # open overview
    sleep(1) # give overview a little time to show

    typeText("Terminal") # search application
    pressKey("Enter") # confirm by Enter

    sleep(1) # Give application a little time to start

    # Load application root to variable
    app = root.application("gnome-terminal-server") # save root object

    # Search the application tree for objects
    app.child("File", "menu").click() # find menu and click
    app.child("New Tab", "menu item").click() # find menu item and click

    # Execute command
    typeText("echo Hello World") # type command to terminal
    pressKey("Enter") # confirm command


Behave
======
To give the tests some structure we use the `behave <https://github.com/behave/behave>`_.

Installing behave via pip (if you are using **qecore**, there is no need to install it manually
as qecore has upstream behave as a dependency)::

    python3 -m pip install behave

Behave expects the following project structure:

.. code-block::

    features
    ├── environment.py
    ├── main.feature
    └── steps
        └── steps.py

* **environment.py** - gives us an option to include environmental control - code to run before and after steps, scenarios or features
* **main.feature** - behave feature using `natural language format <https://behave.readthedocs.io/en/latest/gherkin.html#gherkin-feature-testing-language>`_ (Gherkin), there can be multiple feature files.
* **steps.py** - python step implementations, there can be multiple python files for steps.

Environment
-----------
Simple environment example.

.. code-block:: python

    #!/usr/bin/python3

    def before_all(context):
        do_stuff_before_all()

    def before_scenario(context, scenario):
        do_stuff_before_scenario()

    def after_scenario(context, scenario):
        do_stuff_after_scenario()

Other behave-defined functions for **environment.py** can be found `here <https://behave.readthedocs.io/en/latest/tutorial.html#environmental-controls>`_.

(**do_stuff*()** functions have to be user defined of course.)


Feature Files
-------------
Structures to which we write the individual Tests which are called **Scenarios** in behave.
Each Scenario is then composed of multiple **Steps**.

.. code-block:: gherkin

  Feature: Terminal command execute

    @tag
    @another_tag
    @execute_echo_command
    Scenario: Execute command in Terminal
      * Open GNOME overview
      * Find Application Terminal
      * Open the Application
      * Execute command


Structure of these files is as follows:

.. code-block::

    ┌───────────────────────────────────────────────────────────────────────────────────┐
    │         ┌────────────────────────────────┐ ┌────────────────────────────────┐     │
    │         │          ┌──────┐ ┌──────┐     │ │          ┌──────┐ ┌──────┐     │     │
    │ Feature │ Scenario │ Step │ │ Step │ ... │ │ Scenario │ Step │ │ Step │ ... │ ... │
    │         │          └──────┘ └──────┘     │ │          └──────┘ └──────┘     │     │
    │         └────────────────────────────────┘ └────────────────────────────────┘     │
    └───────────────────────────────────────────────────────────────────────────────────┘

There can be multiple Scenarios in Features and multiple Steps in Scenarios. Each Scenario can have multiple tags.

When writting features you can decide to use asterisk (*) or keywords
**Given**, **When**, **And**, **But**, **Then** for which the asterisk is a replacement.

More about feature files and its options in `behave feature files <https://behave.readthedocs.io/en/latest/tutorial.html#feature-files>`_.

For me personally the asterisk version of Steps is more readable than with the keywords,
but both versions are perfectly correct to use.

Steps
-----
The exact usage and imlementation of steps see `Step Implementation <https://behave.readthedocs.io/en/latest/tutorial.html#python-step-implementations>`_.

In short we implement individual Steps in Scenarios as functions in **steps.py** and tie them together with use of decorators as can be seen bellow:

.. code-block:: python

    #!/usr/bin/python3
    from dogtail.tree import root
    from dogtail.rawinput import typeText, pressKey
    from time import sleep
    from behave import step

    # First open the application
    @step('Open GNOME overview')
    def open_overview(context):
        pressKey("Super") # open overview
        sleep(1) # give overview a little time to show

    # Find the application
    @step('Find Application Terminal')
    def find_app(context):
        typeText("Terminal") # search application

    # Open the application
    @step('Open the Application')
    def open_app(context):
        pressKey("Enter") # confirm by Enter
        sleep(1) # Give application a little time to start

    # Execute command
    @step('Execute command')
    def execute_command(context):
        typeText("echo Hello World") # type command to terminal
        pressKey("Enter") # confirm command

Usage
======

With **environment.py** done, we have defined our Steps in **steps.py** and given them decorators
we can now use in **main.feature**.

For exact behave usage see: `behave usage <https://behave.readthedocs.io/en/latest/behave.html>`_.

In our dogtail session we can now start the test as follows::

    $ behave -kt execute_echo_command

* *behave* is the script which will parse/execute all necessary stuff to get the test going.
* *-k* is option to not print skipped tests (when having defined multiple tests we want to see result only for one when executing one)
* *-t* execute test with given *tag* which in our case is **execute_echo_command**

Since qecore-headless starts the script given - in above case it was bash - we can also start the test as follows::

    $ qecore-headless "behave -kt execute_echo_command"

This ensures the test will start in "fresh session" - starting GDM, executing the test, stoping the GDM
(unless specified otherwise with qecore-headless options).

Output of the test execution than looks like this::

    [test@localhost test]$ behave -kt execute_echo_command
    Feature: Terminal command execute # features/main.feature:1

    @tag @another_tag @execute_echo_command
    Scenario: Execute command in Terminal  # features/main.feature:6
        * Open GNOME overview                # features/steps/steps.py:8 1.104s
        * Find Application Terminal          # features/steps/steps.py:14 0.831s
        * Open the Application               # features/steps/steps.py:19 1.102s
        * Execute command                    # features/steps/steps.py:25 1.729s

    1 feature passed, 0 failed, 0 skipped
    1 scenario passed, 0 failed, 0 skipped
    4 steps passed, 0 failed, 0 skipped, 0 undefined
    Took 0m4.766s
