image\_matching
===============

Image matching is quite complicated at first look and has quite a lot of steps.
So for general use look for pre-coded steps to use.
In other situations when pre-coded steps do not suit you needs you can import its parts and build your own.

You are encouraged to build your own step function according to your needs.

Pre-coded steps that you see bellow are:

* General step that just compares and asserts the result - :py:func:`image_matching.image_match`
* General step that just compares and clicks on the found result - :py:func:`image_matching.locate_and_click`

What is needed for image match:

* You need to capture an image in which we look for the element you want to find:

   * Provided by capture_image method in Matcher class.
   * This option is True by default.
   * If you have your own, set :py:func:`image_matching.Matcher.match` capture=False and provide :py:attr:`image_matching.Matcher.screen_path` in the Matcher class.

* You need to match the two images, you are looking for a 'needle':
   So you provide it in function or in step call (.feature file).

   * Provided by :py:func:`image_matching.Matcher.match` which will return True or False. Lets user react on False return value.
   * Provided by :py:func:`image_matching.Matcher.assert_match` which will assert the result and terminate the test on False.

* (Optional) You can draw the result for attachment or your own confirmation that matching works:

   * Provided by :py:func:`image_matching.Matcher.draw` method on Matcher instance to get an image with highlighted needle.
      Highlight is a red rectangle exactly in a place of a match, surrounding provided needle.

* (Optional) You can click on your found result:

   * Provided by :py:func:`image_matching.Matcher.click` method in Matcher instance.
   * Requirements are of course success of a match/assert_match.

* (Optional) You can embed result to test report:

   * For this option the method :py:func:`image_matching.Matcher.draw` is required.
   * Use method provided by :py:func:`sandbox.TestSandbox.attach_image_to_report`
   * Or embed it on your own:

   .. code-block:: python

      context.embed("image/png", open(image_location, "rb").read(), caption="DefaultCaption")

   * Remember that result is saved in Matcher instance :py:attr:`image_matching.Matcher.diff_path` which equals "/tmp/diff.png"

* (Optional) You can search only in region of screen:
   * Firstly, you have to save the region into context.opencv_region attribute
   * Format of this attribute is tuple (node.position, node.size):

      * position and size are (x, y) tuples of integers

Generated documentation
.......................

.. automodule:: image_matching
   :members:
   :undoc-members:
   :show-inheritance:

